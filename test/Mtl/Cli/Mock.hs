module Mtl.Cli.Mock (run) where

import Control.Monad.State (MonadState, State, execState)
import Lens.Micro (at, non)
import Lens.Micro.GHC ()
import Lens.Micro.Mtl (use, (%=), (.=))

import Mtl.Cli (CliL (..))

import MockState (S, sCredentials, sInput, sTimestamps)

newtype Mock a = Mock {runMock :: State S a}
  deriving newtype (Functor, Applicative, Monad, MonadState S)

instance CliL Mock where
  say _ = pure ()
  input = do
    availableInput <- use sInput
    case availableInput of
      [] -> error "not enough input"
      (x : xs) -> do
        sInput .= xs
        pure x
  login username password = do
    actualPassword <- use (sCredentials . at username)
    pure (actualPassword == Just password)
  readTimestamp file = use (sTimestamps . at file . non 0)
  touch file = sTimestamps . at file . non 0 %= (+ 1)

run :: Mock a -> S -> S
run = execState . runMock

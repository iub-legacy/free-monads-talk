module Free.Cli.Mock (run) where

import Control.Monad.State (State, execState)
import Lens.Micro (at, non)
import Lens.Micro.GHC ()
import Lens.Micro.Mtl (use, (%=), (.=))

import Free.Cli (CliL, CliOp (..))
import Free.Free (foldFree)

import MockState (S, sCredentials, sInput, sTimestamps)

type MockCli = State S

interpretMock :: CliL a -> MockCli a
interpretMock = foldFree exec
 where
  exec :: CliOp r -> MockCli r
  exec Say{} = pure ()
  exec Input = do
    availableInput <- use sInput
    case availableInput of
      [] -> error "not enough input"
      (x : xs) -> do
        sInput .= xs
        pure x
  exec (Login username password) = do
    actualPassword <- use (sCredentials . at username)
    pure (actualPassword == Just password)
  exec (ReadTimestamp file) = use (sTimestamps . at file . non 0)
  exec (Touch file) = sTimestamps . at file . non 0 %= (+ 1)

run :: CliL a -> S -> S
run = execState . interpretMock

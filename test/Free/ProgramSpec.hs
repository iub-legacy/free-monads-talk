{-# LANGUAGE OverloadedLists #-}

module Free.ProgramSpec (spec) where

import Control.Exception (Exception, evaluate)
import Lens.Micro (Lens', at, non, (^.))
import Test.Hspec (Spec, describe, it)
import Test.Hspec.Expectations (
  Expectation,
  HasCallStack,
  Selector,
  anyErrorCall,
  shouldBe,
  shouldSatisfy,
  shouldThrow,
 )

import Free.Program (interactiveLogin)
import Utils (Time, getLoginFile)

import Free.Cli.Mock qualified as Mock (run)
import MockState (S (..), sTimestamps)

spec :: Spec
spec = do
  describe "interactiveLogin" $ do
    it "fails on no input" $ do
      Mock.run interactiveLogin mempty `shouldThrow'` anyErrorCall

    it "doesn't touch the FS on incorrect credentials" $ do
      let initS = antoxaUserS{_sInput = ["antoine", "od"]}
      let finalS = Mock.run interactiveLogin initS
      _sTimestamps finalS `shouldBe` _sTimestamps initS

    it "increases the timestamp" $ do
      let initS = antoxaUserS{_sInput = ["antoxa", "228"]}
      let finalS = Mock.run interactiveLogin initS
      finalS `shouldSatisfy` (\x -> x ^. antoxaTimestamp > initS ^. antoxaTimestamp)

    it "increases the timestamp on correct credentials on a subsequent run" $ do
      let initS = antoxaUserS{_sInput = ["antoxa", "228", "antoxa", "228"]}
      let finalS = Mock.run (interactiveLogin *> interactiveLogin) initS
      finalS `shouldSatisfy` (\x -> x ^. antoxaTimestamp > initS ^. antoxaTimestamp)

shouldThrow' :: (HasCallStack, Exception e) => a -> Selector e -> Expectation
action `shouldThrow'` p = evaluate action `shouldThrow` p

antoxaUserS :: S
antoxaUserS = mempty{_sCredentials = [("antoxa", "228")]}

antoxaTimestamp :: Lens' S Time
antoxaTimestamp = sTimestamps . at (getLoginFile "antoxa") . non 0

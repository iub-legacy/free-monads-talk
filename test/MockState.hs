module MockState (S (..), sInput, sTimestamps, sCredentials) where

import Data.Map (Map)
import Lens.Micro (Lens')
import Lens.Micro.GHC ()

import Utils (Password, Time, Username)

data S = State
  { _sInput :: [String]
  , _sTimestamps :: Map FilePath Time
  , _sCredentials :: Map Username Password
  }
  deriving stock (Show)

instance Semigroup S where
  s1 <> s2 =
    State
      { _sInput = _sInput s1 <> _sInput s2
      , _sTimestamps = _sTimestamps s1 <> _sTimestamps s2
      , _sCredentials = _sCredentials s1 <> _sCredentials s2
      }

instance Monoid S where
  mempty = State{_sInput = mempty, _sTimestamps = mempty, _sCredentials = mempty}

sInput :: Lens' S [String]
sInput lMod g = fmap (\x -> g{_sInput = x}) (lMod (_sInput g))

sTimestamps :: Lens' S (Map FilePath Time)
sTimestamps lMod g = fmap (\x -> g{_sTimestamps = x}) (lMod (_sTimestamps g))

sCredentials :: Lens' S (Map Username Password)
sCredentials lMod g = fmap (\x -> g{_sCredentials = x}) (lMod (_sCredentials g))

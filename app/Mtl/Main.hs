module Main (main) where

import Mtl.Cli.Run qualified as Cli (run)
import Mtl.Program (interactiveLogin)

main :: IO ()
main = Cli.run interactiveLogin

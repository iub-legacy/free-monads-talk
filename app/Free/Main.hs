module Main (main) where

import Free.Cli.Run qualified as Cli (run)
import Free.Program (interactiveLogin)

main :: IO ()
main = Cli.run interactiveLogin

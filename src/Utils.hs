module Utils (
  Username,
  Password,
  Time,
  getLoginFile,
  prettyShow,
  toUTCTime,
) where

import Data.Time (UTCTime)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Data.Time.Format (defaultTimeLocale, formatTime, rfc822DateFormat)
import System.FilePath ((</>))
import System.Posix.Types (EpochTime)

type Username = String
type Password = String
type Time = EpochTime

loginDir :: FilePath
loginDir = "/tmp"

getLoginFile :: Username -> FilePath
getLoginFile username = loginDir </> username

prettyShow :: Time -> String
prettyShow = formatTime defaultTimeLocale rfc822DateFormat . toUTCTime

toUTCTime :: Time -> UTCTime
toUTCTime = posixSecondsToUTCTime . realToFrac

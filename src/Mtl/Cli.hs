module Mtl.Cli (CliL (..)) where

import Utils (Password, Time, Username)

class Monad m => CliL m where
  say :: String -> m ()
  input :: m String
  login :: Username -> Password -> m Bool
  readTimestamp :: FilePath -> m Time
  touch :: FilePath -> m ()

module Mtl.Cli.Run (run) where

import Control.Exception (catch, throw)
import Control.Monad (unless, void)
import System.IO.Error (isDoesNotExistError)
import System.Posix.Files (getFileStatus, modificationTime, regularFileMode, touchFile)
import System.Posix.IO (createFile)
import System.Posix.PAM qualified as PAM

import Mtl.Cli (CliL (..))

{-
'Run' has to be a newtype to avoid an orphan instance. I could have
defined the instance in Mtl.Cli, but in general we can't modify the
original source code, or prefer not to for reasons of modularity. So,
it's more indicative of the common case to do it like that.
-}
newtype Run a = Run {run :: IO a}
  deriving newtype (Functor, Applicative, Monad)

instance CliL Run where
  say = Run . putStrLn
  input = Run getLine
  login username password = Run $ do
    retCode <- PAM.authenticate "system-local-login" username password
    unless (PAM.isSuccess retCode) $ do
      putStrLn (PAM.pamCodeToMessage retCode)
    pure (PAM.isSuccess retCode)
  readTimestamp file = Run $ do
    catch (modificationTime <$> getFileStatus file) $ \e ->
      if isDoesNotExistError e then pure 0 else throw e
  touch file = Run $ do
    catch (touchFile file) $ \e ->
      if isDoesNotExistError e
        then void (createFile file regularFileMode)
        else throw e

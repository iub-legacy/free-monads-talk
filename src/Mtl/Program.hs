module Mtl.Program (getLoginFile, interactiveLogin) where

import Mtl.Cli (CliL (..))
import Utils (getLoginFile, prettyShow)

interactiveLogin :: CliL m => m ()
interactiveLogin = do
  say "enter the username:"
  username <- input
  say "enter the password:"
  password <- input
  loginOk <- login username password
  if loginOk
    then do
      let loginFile = getLoginFile username
      lastLoginTime <- readTimestamp loginFile
      touch loginFile
      say ("Logged in, last login was at " <> prettyShow lastLoginTime)
    else do
      say "Incorrect username or password"

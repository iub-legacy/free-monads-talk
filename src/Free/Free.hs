module Free.Free (Free, liftFree, foldFree) where

import Control.Applicative (liftA2)
import Control.Monad (liftM, liftM2)

data Free f b where
  Pure :: b -> Free f b
  Step :: f a -> (a -> Free f b) -> Free f b

instance Functor (Free f) where
  fmap = liftM

instance Applicative (Free f) where
  pure = Pure
  liftA2 = liftM2

instance Monad (Free f) where
  Pure x >>= f = f x
  Step fx cont >>= f = Step fx (\x -> cont x >>= f)

liftFree :: f a -> Free f a
liftFree f = Step f pure

foldFree ::
  Monad m => (forall r. f r -> m r) -> Free f a -> m a
foldFree _exec (Pure x) = pure x
foldFree exec (Step fx cont) = do
  x <- exec fx
  foldFree exec (cont x)

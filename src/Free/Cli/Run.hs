module Free.Cli.Run (run) where

import Control.Exception (catch, throw)
import Control.Monad (unless, void)
import System.IO.Error (isDoesNotExistError)
import System.Posix.Files (getFileStatus, modificationTime, regularFileMode, touchFile)
import System.Posix.IO (createFile)
import System.Posix.PAM qualified as PAM

import Free.Cli (CliL, CliOp (..))
import Free.Free (foldFree)

interpret :: CliL a -> IO a
interpret = foldFree exec
 where
  exec :: CliOp r -> IO r
  exec (Say what) = putStrLn what
  exec Input = getLine
  exec (Login username password) = do
    retCode <- PAM.authenticate "system-local-login" username password
    unless (PAM.isSuccess retCode) $ do
      putStrLn (PAM.pamCodeToMessage retCode)
    pure (PAM.isSuccess retCode)
  exec (ReadTimestamp file) =
    catch (modificationTime <$> getFileStatus file) $ \e ->
      if isDoesNotExistError e then pure 0 else throw e
  exec (Touch file) =
    catch (touchFile file) $ \e ->
      if isDoesNotExistError e
        then void (createFile file regularFileMode)
        else throw e

run :: CliL a -> IO a
run = interpret

module Free.Program (interactiveLogin) where

import Free.Cli (CliL, input, login, readTimestamp, say, touch)
import Utils (getLoginFile, prettyShow)

interactiveLogin :: CliL ()
interactiveLogin = do
  say "enter the username:"
  username <- input
  say "enter the password:"
  password <- input
  loginOk <- login username password
  if loginOk
    then do
      let loginFile = getLoginFile username
      lastLoginTime <- readTimestamp loginFile
      touch loginFile
      say ("Logged in, last login was at " <> prettyShow lastLoginTime)
    else do
      say "Incorrect username or password"

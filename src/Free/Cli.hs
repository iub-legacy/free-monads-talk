module Free.Cli (
  CliL,
  CliOp (..),
  say,
  input,
  login,
  readTimestamp,
  touch,
) where

import Free.Free (Free, liftFree)
import Utils (Password, Time, Username)

data CliOp a where
  Say :: String -> CliOp ()
  Input :: CliOp String
  Login :: Username -> Password -> CliOp Bool
  ReadTimestamp :: FilePath -> CliOp Time
  Touch :: FilePath -> CliOp ()
type CliL = Free CliOp

say :: String -> CliL ()
say what = liftFree (Say what)

input :: CliL String
input = liftFree Input

login :: Username -> Password -> CliL Bool
login username password = liftFree (Login username password)

readTimestamp :: FilePath -> CliL Time
readTimestamp file = liftFree (ReadTimestamp file)

touch :: FilePath -> CliL ()
touch file = liftFree (Touch file)
